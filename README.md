NePlus Database
===============

Custom modifications on top of Nette Database.


Usage
-----
```php
// constructor is the same as of Nette\Database\Connection
$db = ElektroPotkan\NePlus\Database\ConnectionMySQL(
	'mysql:host=localhost;dbname=mydb',
	'user',
	'password'
);

// query LOCK TABLES `myTable1` READ, `myTable2` WRITE
$db->lockTables([
	'myTable1' => false,
	'myTable2' => true,
]);

// query UNLOCK TABLES
$db->unlockTables();
```

### Nette DI
To switch all database connections registered in DI Container
from `Nette\Database\Connection` to `ElektroPotkan\NePlus\Database\ConnectionMySQL`,
just use the bundled DI extension:
```neon
extensions:
	- ElektroPotkan\NePlus\Bridges\Database\DI\DatabaseExtension
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
