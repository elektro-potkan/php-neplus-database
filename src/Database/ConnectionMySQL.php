<?php

declare(strict_types=1);

namespace ElektroPotkan\NePlus\Database;

use InvalidArgumentException;
use LogicException;
use Nette\Database\Connection;
use Nette\Database\ResultSet;
use PDO;


/**
 * Light enhancement of standard Nette DB Connection class
 * Compatible with MySQL only!
 */
class ConnectionMySQL extends Connection {
	/** Transaction ISOLATION LEVELs */
	public const
		TIL_READ_UNCOMMITTED = 'READ UNCOMMITTED',
		TIL_READ_COMMITTED = 'READ COMMITTED',
		TIL_REPEATABLE_READ = 'REPEATABLE READ',
		TIL_SERIALIZABLE = 'SERIALIZABLE';
	protected const
		TILs = [
			self::TIL_READ_UNCOMMITTED,
			self::TIL_READ_COMMITTED,
			self::TIL_REPEATABLE_READ,
			self::TIL_SERIALIZABLE,
		];
	
	/** Tables lock-types */
	public const
		LT_READ = 'READ',
		LT_READ_LOCAL = 'READ LOCAL',
		LT_WRITE = 'WRITE',
		LT_WRITE_CONCURRENT = 'WRITE CONCURRENT',
		LT_WRITE_LOW_PRIORITY = 'LOW_PRIORITY WRITE';
	protected const
		LTs = [
			self::LT_READ,
			self::LT_READ_LOCAL,
			self::LT_WRITE,
			self::LT_WRITE_CONCURRENT,
			self::LT_WRITE_LOW_PRIORITY,
		];
	
	
	/** @var int */
	private $transactionDepth = 0;
	
	
	/**
	 * Checks whether this class can support the connection it is using
	 * @param bool $throw - whether to throw an exception when not supported
	 * @throws LogicException
	 */
	public function isDriverSupported(bool $throw = true): bool {
		$supported = ($this->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql');
		
		if(!$supported && $throw){
			throw new LogicException('Class ' . self::class . ' could be used for MySQL connections only!');
		};
		
		return $supported;
	} // isDriverSupported
	
	
	/********************* tables locking *********************/
	
	/**
	 * Simplifies LOCK TABLES query
	 * @param non-empty-array<string, bool|string> $tables - array with table names as keys and lock-type as in LT_* class constants as value
	 *   optionally, a boolean can be used as value to specify lock-type (false = READ, true = WRITE)
	 * @param ?int $wait - whether to append (NO)WAIT clause
	 */
	public function lockTables(array $tables, ?int $wait = null): ResultSet {
		$this->isDriverSupported();
		
		if(count($tables) < 1){
			throw new InvalidArgumentException('Nothing to do!');
		};
		
		$sql = [];
		$args = [];
		foreach($tables as $name => $op){
			if(!is_string($name) || trim($name) === ''){
				throw new InvalidArgumentException('Table name must be non-empty string!');
			};
			
			if(is_bool($op)){
				$op = $op ? self::LT_WRITE : self::LT_READ;
			};
			
			if(!in_array($op, self::LTs, true)){
				throw new LogicException('Invalid lock-type given!');
			};
			
			$sql[] = '?name ' . $op;
			$args[] = $name;
		};
		
		if($wait !== null){
			if($wait > 0){
				$args[] = 'WAIT ?';
				$args[] = $wait;
			}
			else {
				$args[] = 'NOWAIT';
			};
		};
		
		return $this->query('LOCK TABLES '.implode(', ', $sql), ...$args);
	} // lockTables
	
	/**
	 * Shortcut for UNLOCK TABLES query
	 */
	public function unlockTables(): ResultSet {
		$this->isDriverSupported();
		
		return $this->query('UNLOCK TABLES');
	} // unlockTables
	
	
	/********************* transactions *********************/
	
	/**
	 * Sets transaction properties
	 * @param ?string $isolationLevel - transaction ISOLATION LEVEL as in TIL_* class constants (null to ommit it from the call)
	 * @param ?bool $writable - whether to include hints about transaction writability
	 *   - true = READ WRITE
	 *   - false = READ ONLY
	 *   - null = no hints included in the call
	 * @param bool $session - true to set the defaults for this session, false to affect the next transaction only
	 */
	public function setTransaction(?string $isolationLevel = null, ?bool $writable = null, bool $session = false): void {
		if($isolationLevel === null && $writable === null){
			throw new LogicException('At least one transaction property must be set!');
		};
		
		$sql = 'SET';
		if($session){
			$sql .= ' SESSION';
		};
		$sql .= ' TRANSACTION ';
		
		$properties = [];
		
		if($isolationLevel !== null){
			if(!in_array($isolationLevel, self::TILs, true)){
				throw new LogicException('Invalid ISOLATION LEVEL given!');
			};
			
			$properties[] = 'ISOLATION LEVEL ' . $isolationLevel;
		};
		
		if($writable !== null){
			$properties[] = $writable ? 'READ WRITE' : 'READ ONLY';
		};
		
		$sql .= implode(', ', $properties);
		
		$this->query($sql);
	} // setTransaction
	
	/**
	 * Starts transaction
	 * @param ?bool $writable - whether to include hints about transaction writability
	 *   - true = READ WRITE
	 *   - false = READ ONLY
	 *   - null = no hints included in the call
	 * @param bool $snapshot - whether to request consistent snapshot for transaction (false ommits it from the call)
	 * @param ?string $isolationLevel - transaction ISOLATION LEVEL as in TIL_* class constants (null to ommit it from the call)
	 */
	public function beginTransaction(?bool $writable = null, bool $snapshot = false, ?string $isolationLevel = null): void {
		if($this->transactionDepth !== 0){
			throw new LogicException(__METHOD__ . '() call is forbidden inside a transaction() callback');
		};
		
		if($isolationLevel !== null){
			$this->setTransaction($isolationLevel);
		};
		
		$properties = [];
		
		if($snapshot){
			$properties[] = 'WITH CONSISTENT SNAPSHOT';
		};
		
		if($writable !== null){
			$properties[] = $writable ? 'READ WRITE' : 'READ ONLY';
		};
		
		$sql = 'START TRANSACTION';
		if($properties !== []){
			$sql .= ' ' . implode(', ', $properties);
		};
		
		$this->query($sql);
	} // beginTransaction
	
	/**
	 * Commits transaction
	 * @param ?bool $chain - whether to request transactions chaining
	 *   - true = explicitly CHAIN
	 *   - false = explicitly NO CHAIN
	 *   - null = not included in the call (uses server default)
	 * @param ?bool $release - whether to request disconnection of client
	 *   - true = explicitly RELEASE
	 *   - false = explicitly NO RELEASE
	 *   - null = not included in the call (uses server default)
	 */
	public function commit(?bool $chain = null, ?bool $release = null): void {
		if($this->transactionDepth !== 0){
			throw new LogicException(__METHOD__ . '() call is forbidden inside a transaction() callback');
		};
		
		$sql = 'COMMIT';
		
		if($chain !== null){
			$sql .= ' AND';
			
			if(!$chain){
				$sql .= ' NO';
			};
			
			$sql .= ' CHAIN';
		};
		
		if($release !== null){
			if(!$release){
				$sql .= ' NO';
			};
			
			$sql .= ' RELEASE';
		};
		
		$this->query($sql);
	} // commit
	
	/**
	 * Rollbacks transaction
	 * @param ?bool $chain - whether to request transactions chaining
	 *   - true = explicitly CHAIN
	 *   - false = explicitly NO CHAIN
	 *   - null = not included in the call (uses server default)
	 * @param ?bool $release - whether to request disconnection of client
	 *   - true = explicitly RELEASE
	 *   - false = explicitly NO RELEASE
	 *   - null = not included in the call (uses server default)
	 */
	public function rollBack(?bool $chain = null, ?bool $release = null): void {
		if($this->transactionDepth !== 0){
			throw new LogicException(__METHOD__ . '() call is forbidden inside a transaction() callback');
		};
		
		$sql = 'ROLLBACK';
		
		if($chain !== null){
			$sql .= ' AND';
			
			if(!$chain){
				$sql .= ' NO';
			};
			
			$sql .= ' CHAIN';
		};
		
		if($release !== null){
			if(!$release){
				$sql .= ' NO';
			};
			
			$sql .= ' RELEASE';
		};
		
		$this->query($sql);
	} // rollBack
	
	/**
	 * @return mixed
	 */
	public function transaction(callable $callback){
		$this->transactionDepth++;
		
		try {
			return parent::transaction($callback);
		}
		finally {
			$this->transactionDepth--;
		};
	} // transaction
} // class ConnectionMySQL
