<?php

declare(strict_types=1);

namespace ElektroPotkan\NePlus\Bridges\Database\DI;

use Nette\Database\Connection;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\ServiceDefinition;

use ElektroPotkan\NePlus\Database\ConnectionMySQL;


/**
 * Override Nette\Database\Connection type to its descendant ElektroPotkan\NePlus\Database\ConnectionMySQL
 */
class DatabaseExtension extends CompilerExtension {
	public function beforeCompile(): void {
		$builder = $this->getContainerBuilder();
		
		foreach($builder->findByType(Connection::class) as $def){
			if($def instanceof ServiceDefinition){
				$def->setFactory(ConnectionMySQL::class, $def->getFactory()->arguments);
				$def->setType(ConnectionMySQL::class);
			};
		};
	} // beforeCompile
} // class DatabaseExtension
